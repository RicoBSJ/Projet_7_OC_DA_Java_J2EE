package com.aubrun.eric.projet7.business;

import com.aubrun.eric.projet7.beans.*;
import com.aubrun.eric.projet7.business.mapper.BookDtoMapper;
import com.aubrun.eric.projet7.business.service.BookService;
import com.aubrun.eric.projet7.consumer.repository.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Sort;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

@SpringBootTest
public class BookServiceTest {

    @Autowired
    private BookService bookService;
    @MockBean
    private BorrowingRepository borrowingRepository;
    @MockBean
    private BookRepository bookRepository;
    @MockBean
    private UserAccountRepository userAccountRepository;
    @MockBean
    private ReservationRepository reservationRepository;
    @MockBean
    private AuthorRepository authorRepository;
    @MockBean
    private SearchRepository searchRepository;
    @MockBean
    private CategoryRepository categoryRepository;
    @MockBean
    private EditionRepository editionRepository;
    @MockBean
    private RoleRepository roleRepository;

    static private List<Book> bookList;
    static private Book book;
    static private SearchBook searchBook;

    @BeforeAll
    static void setUpBefore() {

        Author author = new Author();
        author.setAuthorId(2);
        author.setFirstName("Eliyahu M.");
        author.setLastName("Goldratt");

        Category category = new Category();
        category.setCategoryId(1);
        category.setNameCategory("management");

        Edition edition = new Edition();
        edition.setEditionId(2);
        edition.setNameEdition("Les Belles Lettres");

        UserAccount userAccount = new UserAccount();
        userAccount.setUserId(1);
        userAccount.setUsername("Musk");
        userAccount.setEmail("aubrun.eric@free.fr");
        userAccount.setPassword("$2a$10$PBcySTS3eGN69mRzF6BWx.hs7aayN0FIJqLY16ZsKen4S0ikAyL9G");

        book = new Book();
        book.setBookId(16);
        book.setQuantity(1);
        book.setMaxQuantity(1);
        book.setTitle("Le but");
        book.setYearBook("2013-04-19");
        book.setBookAuthor(author);
        book.setBookCategory(category);
        book.setBookEdition(edition);

        Reservation reservation = new Reservation();
        reservation.setReservationId(1);
        reservation.setUser(userAccount);
        reservation.setBook(book);

        List<Reservation> reservations = new ArrayList<>();
        reservations.add(reservation);

        book.setReservationWaitingList(reservations);

        searchBook = new SearchBook();
        searchBook.setSearchBookId(6);
        searchBook.setSearchBookTitle("Le but");
        searchBook.setSearchBookReleaseDate("2013-04-19");
        searchBook.setSearchBookAuthorName(author.getLastName());
        searchBook.setSearchBookPublishingHouse(edition.getNameEdition());

        bookList = new ArrayList<>();
        bookList.add(book);
    }

    @Test
    public void findAll() {
        when(bookRepository.findAll((Sort) Mockito.any())).thenReturn(bookList);
        bookService.findAll();
        Optional<Book> bookByBookId = bookRepository.findBookByBookId(book.getBookId());
        assertNotNull(bookByBookId, () -> "The bookByBookId should not be null");
    }

    @Test
    public void save() {
        when(bookRepository.save(Mockito.any())).thenReturn(book);
        bookService.save(BookDtoMapper.toDto(book));
        Optional<Book> bookByBook = bookRepository.findBookByBookId(book.getBookId());
        assertNotNull(bookByBook, () -> "The bookByBook should not be null");
    }

    @Test
    public void update() {
        when(bookRepository.save(Mockito.any())).thenReturn(book);
        bookService.update(BookDtoMapper.toDto(book));
        Optional<Book> bookByBook = bookRepository.findBookByBookId(book.getBookId());
        assertNotNull(bookByBook, () -> "The bookByBook should not be null");
    }

    @Test
    public void findById() {
        when(bookRepository.findById(book.getBookId())).thenReturn(Optional.ofNullable(book));
        bookService.findById(book.getBookId());
        Optional<Book> bookByBook = bookRepository.findBookByBookId(book.getBookId());
        assertNotNull(bookByBook, () -> "The bookByBook should not be null");
    }

    @Test
    public void delete() {
        when(bookRepository.findById(book.getBookId())).thenReturn(Optional.ofNullable(book));
        bookService.delete(book.getBookId());
        final Runnable runnable = () -> bookRepository.delete(book);
        assertNotNull(runnable, () -> "The runnable should not be null");
    }

    @Test
    public void searchBook() {
        when(bookRepository.findById(book.getBookId())).thenReturn(Optional.ofNullable(book));
        bookService.searchBook(searchBook);
        final List<Book> titleAndBookAuthorAndBookEdition = searchRepository.findAllByTitleAndBookAuthorAndBookEdition(searchBook);
        assertNotNull(titleAndBookAuthorAndBookEdition, () -> "The titleAndBookAuthorAndBookEdition should not be null");
    }
}