package com.aubrun.eric.projet7.business;

import com.aubrun.eric.projet7.beans.*;
import com.aubrun.eric.projet7.business.mapper.UserAccountDtoMapper;
import com.aubrun.eric.projet7.business.service.UserAccountService;
import com.aubrun.eric.projet7.consumer.repository.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

@SpringBootTest
public class UserAccountServiceTest {

    @Autowired
    private UserAccountService userAccountService;
    @MockBean
    private BorrowingRepository borrowingRepository;
    @MockBean
    private BookRepository bookRepository;
    @MockBean
    private UserAccountRepository userAccountRepository;
    @MockBean
    private ReservationRepository reservationRepository;
    @MockBean
    private AuthorRepository authorRepository;
    @MockBean
    private SearchRepository searchRepository;
    @MockBean
    private CategoryRepository categoryRepository;
    @MockBean
    private EditionRepository editionRepository;
    @MockBean
    private RoleRepository roleRepository;

    static private UserAccount userAccount;
    static private List<UserAccount> userAccountList;

    @BeforeAll
    static void setUpBefore() {
        userAccount = new UserAccount();
        userAccount.setUserId(1);
        userAccount.setUsername("Musk");
        userAccount.setEmail("aubrun.eric@free.fr");
        userAccount.setPassword("$2a$10$PBcySTS3eGN69mRzF6BWx.hs7aayN0FIJqLY16ZsKen4S0ikAyL9G");

        userAccountList = new ArrayList<>();
        userAccountList.add(userAccount);
    }

    @Test
    public void findAll() {
        when(userAccountRepository.findAll()).thenReturn(userAccountList);
        userAccountService.findAll();
        final List<UserAccount> userAccounts = userAccountRepository.findAll();
        assertNotNull(userAccounts, () -> "The userAccounts should not be null");
    }

    @Test
    public void save() {
        when(userAccountRepository.save(Mockito.any())).thenReturn(userAccount);
        userAccountService.save(UserAccountDtoMapper.toDto(userAccount));
        final Optional<UserAccount> byId = userAccountRepository.findById(userAccount.getUserId());
        assertNotNull(byId, () -> "The save should not be null");
    }

    @Test
    public void update() {
        when(userAccountRepository.save(Mockito.any())).thenReturn(userAccount);
        userAccountService.update(UserAccountDtoMapper.toDto(userAccount));
        final UserAccount save = userAccountRepository.save(userAccount);
        assertNotNull(save, () -> "The save should not be null");
    }

    @Test
    public void findById() {
        when(userAccountRepository.findById(userAccount.getUserId())).thenReturn(Optional.ofNullable(userAccount));
        userAccountService.findById(userAccount.getUserId());
        final Optional<UserAccount> byId = userAccountRepository.findById(userAccount.getUserId());
        assertNotNull(byId, () -> "The save should not be null");
    }

    @Test
    public void delete() {
        when(userAccountRepository.findById(userAccount.getUserId())).thenReturn(Optional.ofNullable(userAccount));
        userAccountService.delete(userAccount.getUserId());
        final Runnable runnable = () -> userAccountRepository.deleteById(userAccount.getUserId());
        assertNotNull(runnable, () -> "The save should not be null");
    }
}
