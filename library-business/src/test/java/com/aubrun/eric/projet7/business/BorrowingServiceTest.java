package com.aubrun.eric.projet7.business;

import com.aubrun.eric.projet7.beans.*;
import com.aubrun.eric.projet7.business.dto.BorrowingDto;
import com.aubrun.eric.projet7.business.service.BorrowingService;
import com.aubrun.eric.projet7.consumer.repository.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.security.Principal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@SpringBootTest
public class BorrowingServiceTest {

    @Autowired
    private BorrowingService borrowingService;
    @MockBean
    private BorrowingRepository borrowingRepository;
    @MockBean
    private BookRepository bookRepository;
    @MockBean
    private UserAccountRepository userAccountRepository;
    @MockBean
    private ReservationRepository reservationRepository;
    @MockBean
    private AuthorRepository authorRepository;
    @MockBean
    private SearchRepository searchRepository;
    @MockBean
    private CategoryRepository categoryRepository;
    @MockBean
    private EditionRepository editionRepository;
    @MockBean
    private RoleRepository roleRepository;
    @MockBean
    private Principal principal;

    static private UserAccount userAccount;
    static private Reservation reservation;
    static private Borrowing borrowing;
    static private List<Reservation> reservationList;
    static private List<Borrowing> borrowingList;

    @BeforeAll
    static void setUpBefore() {

        Author author = new Author();
        author.setAuthorId(2);
        author.setFirstName("Eliyahu M.");
        author.setLastName("Goldratt");

        Category category = new Category();
        category.setCategoryId(1);
        category.setNameCategory("management");

        Edition edition = new Edition();
        edition.setEditionId(2);
        edition.setNameEdition("Les Belles Lettres");

        userAccount = new UserAccount();
        userAccount.setUserId(1);
        userAccount.setUsername("Musk");
        userAccount.setEmail("aubrun.eric@free.fr");
        userAccount.setPassword("$2a$10$PBcySTS3eGN69mRzF6BWx.hs7aayN0FIJqLY16ZsKen4S0ikAyL9G");

        Book book = new Book();
        book.setBookId(6);
        book.setQuantity(1);
        book.setMaxQuantity(1);
        book.setTitle("Le but");
        book.setYearBook("2013-04-19");
        book.setBookAuthor(author);
        book.setBookCategory(category);
        book.setBookEdition(edition);

        reservation = new Reservation();
        reservation.setReservationId(1);
        reservation.setUser(userAccount);
        reservation.setBook(book);

        reservationList = new ArrayList<>();
        reservationList.add(reservation);

        book.setReservationWaitingList(reservationList);

        borrowing = new Borrowing();
        borrowing.setBorrowingId(1);
        borrowing.setBookBorrowing(book);
        borrowing.setUserAccountBorrowing(userAccount);
        borrowing.setBeginDate(LocalDate.now());
        borrowing.setEndDate(LocalDate.now().plusWeeks(4));
        borrowing.setRenewal(false);
        borrowing.setRendering(true);

        borrowingList = new ArrayList<>();
        borrowingList.add(borrowing);

        userAccount.setBorrowingList(borrowingList);
        userAccount.setReservationList(reservationList);
    }

    @BeforeEach
    public void principal() {
        when(principal.getName()).thenReturn(userAccount.getUsername());
    }

    @Test
    public void findAllTest() {
        when(userAccountRepository.findByUsername(principal.getName())).thenReturn(Optional.ofNullable(userAccount));
        when(borrowingRepository.findAllUserBorrowing(userAccount)).thenReturn(borrowingList);
        final List<BorrowingDto> borrowingServiceAll = borrowingService.findAll(principal);
        assertNotNull(borrowingServiceAll, () -> "The borrowingServiceAll should not be null");
    }

    @Test
    public void findByIdTest() {
        when(borrowingRepository.findById(borrowing.getBorrowingId())).thenReturn(Optional.ofNullable(borrowing));
        borrowingService.findById(borrowing.getBorrowingId());
        final Optional<Borrowing> byId = borrowingRepository.findById(borrowing.getBorrowingId());
        assertThat(byId).contains(borrowing);
    }

    @Test
    public void deleteReservationTest() {
        when(reservationRepository.findAll()).thenReturn(reservationList);
        borrowingService.deleteReservation(reservation.getReservationId());
        final Runnable runnable = () -> borrowingRepository.deleteById(reservation.getReservationId());
        assertNotNull(runnable, () -> "The save should not be null");
    }

    @Test
    public void updateTest() {
        when(borrowingRepository.findById(borrowing.getBorrowingId())).thenReturn(Optional.ofNullable(borrowing));
        borrowingService.update(borrowing.getBorrowingId());
        final Optional<Borrowing> result = borrowingRepository.findById(borrowing.getBorrowingId());
        assertThat(result.get().getEndDate()).isEqualTo(LocalDate.now().plusWeeks(8));
    }
}
