package com.aubrun.eric.projet7.business.service.exception;

import com.aubrun.eric.projet7.beans.annotations.ExcludeClassFromJacocoGeneratedReport;

@ExcludeClassFromJacocoGeneratedReport
public class WrongIdentifier extends RuntimeException {

    public WrongIdentifier() {
        super("Please verify your login details");
    }


}
