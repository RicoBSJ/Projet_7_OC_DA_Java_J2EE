package com.aubrun.eric.projet7.business.mapper;

import com.aubrun.eric.projet7.beans.Reservation;
import com.aubrun.eric.projet7.beans.annotations.ExcludeClassFromJacocoGeneratedReport;
import com.aubrun.eric.projet7.business.dto.ReservationDto;

@ExcludeClassFromJacocoGeneratedReport
public class ReservationDtoMapper {

    static public ReservationDto toDto(Reservation reservation) {

        ReservationDto dto = new ReservationDto();
        dto.setReservationId(reservation.getReservationId());
        dto.setBook(BookDtoMapper.toDto(reservation.getBook()));
        dto.setUser(UserAccountDtoMapper.toDto(reservation.getUser()));
        dto.setDate(reservation.getDate());
        return dto;
    }

    static public Reservation toEntity(ReservationDto reservationDto) {

        Reservation entity = new Reservation();
        entity.setReservationId(reservationDto.getReservationId());
        entity.setBook(BookDtoMapper.toEntity(reservationDto.getBook()));
        entity.setUser(UserAccountDtoMapper.toEntity(reservationDto.getUser()));
        entity.setDate(reservationDto.getDate());
        return entity;
    }
}
