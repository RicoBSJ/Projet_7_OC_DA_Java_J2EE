package com.aubrun.eric.projet7.business.dto;

import com.aubrun.eric.projet7.beans.annotations.ExcludeClassFromJacocoGeneratedReport;

import java.util.List;

@ExcludeClassFromJacocoGeneratedReport
public class BatchDto {

    private String username;
    private String email;
    private List<BorrowingDto> borrowings;
    private List<ReservationDto> reservations;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<BorrowingDto> getBorrowings() {
        return borrowings;
    }

    public void setBorrowings(List<BorrowingDto> borrowings) {
        this.borrowings = borrowings;
    }

    public List<ReservationDto> getReservations() {
        return reservations;
    }

    public void setReservations(List<ReservationDto> reservations) {
        this.reservations = reservations;
    }
}
