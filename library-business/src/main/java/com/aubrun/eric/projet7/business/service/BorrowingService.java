package com.aubrun.eric.projet7.business.service;

import com.aubrun.eric.projet7.beans.Book;
import com.aubrun.eric.projet7.beans.Borrowing;
import com.aubrun.eric.projet7.beans.Reservation;
import com.aubrun.eric.projet7.beans.UserAccount;
import com.aubrun.eric.projet7.beans.annotations.ExcludeFromJacocoGeneratedReport;
import com.aubrun.eric.projet7.business.dto.*;
import com.aubrun.eric.projet7.business.mapper.BorrowingDtoMapper;
import com.aubrun.eric.projet7.business.mapper.ReservationDtoMapper;
import com.aubrun.eric.projet7.consumer.repository.BookRepository;
import com.aubrun.eric.projet7.consumer.repository.BorrowingRepository;
import com.aubrun.eric.projet7.consumer.repository.ReservationRepository;
import com.aubrun.eric.projet7.consumer.repository.UserAccountRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.security.Principal;
import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;

@Service
@Transactional
public class BorrowingService {

    private final BorrowingRepository borrowingRepository;
    private final BookRepository bookRepository;
    private final UserAccountRepository userAccountRepository;
    private final EmailServiceImpl emailServiceImpl;
    private final ReservationRepository reservationRepository;

    public BorrowingService(BorrowingRepository borrowingRepository, BookRepository bookRepository, UserAccountRepository userAccountRepository, EmailServiceImpl emailServiceImpl, ReservationRepository reservationRepository) {
        this.borrowingRepository = borrowingRepository;
        this.bookRepository = bookRepository;
        this.userAccountRepository = userAccountRepository;
        this.emailServiceImpl = emailServiceImpl;
        this.reservationRepository = reservationRepository;
    }

    public List<BorrowingDto> findAll(Principal principal) {
        UserAccount userAccount = userAccountRepository.findByUsername(principal.getName()).orElseThrow(RuntimeException::new);
        return borrowingRepository.findAllUserBorrowing(userAccount).stream().map(BorrowingDtoMapper::toDto).collect(toList());
    }

    @ExcludeFromJacocoGeneratedReport
    public List<ReservationDto> findAllReserves(Principal principal) {
        UserAccount userAccount = userAccountRepository.findByUsername(principal.getName()).orElseThrow(RuntimeException::new);
        List<ReservationDto> dtos = userAccount.getReservationList().stream().map(ReservationDtoMapper::toDto).collect(toList());
        dtos = dtos.stream().map(this::calculateUserPosition).collect(Collectors.toList());
        dtos = dtos.stream().map(this::nearestEndDateBorrowing).collect(Collectors.toList());
        return dtos;
    }

    @ExcludeFromJacocoGeneratedReport
    private ReservationDto nearestEndDateBorrowing(ReservationDto reservationDto) {
        Integer bookId = reservationDto.getBook().getBookId();
        LocalDate returnDate = Optional.ofNullable(borrowingRepository.findBorrowingByEndDateAndAndBookBorrowing(bookId)).map(Date::toLocalDate).orElse(null);
        reservationDto.setFirstReturnDate(returnDate);
        return reservationDto;
    }

    @ExcludeFromJacocoGeneratedReport
    private ReservationDto calculateUserPosition(ReservationDto reservationDto) {
        Integer bookId = reservationDto.getBook().getBookId();
        Integer id = reservationDto.getUser().getUserId();
        List<Reservation> reservationWaitingList = bookRepository.findBookByBookId(bookId).orElseThrow(RuntimeException::new).getReservationWaitingList();
        int position = 0;
        for (int i = 0; i < reservationWaitingList.size(); i++) {
            if (reservationWaitingList.get(i).getUser().getUserId().equals(id)) {
                position = i + 1;
            }
        }
        reservationDto.setPosition(position);
        return reservationDto;
    }

    public BorrowingDto findById(int borrowingId) {
        return BorrowingDtoMapper.toDto(borrowingRepository.findById(borrowingId).get());
    }

    @ExcludeFromJacocoGeneratedReport
    public void save(int bookId, String userName) {
        UserAccount userAccount = userAccountRepository.findByUsername(userName).get();
        Book book = bookRepository.findBookByBookId(bookId).get();
        final Optional<Borrowing> optionalBorrowing = userAccount.getBorrowingList().stream().filter(b -> b.getBookBorrowing().getBookId().equals(bookId)).findAny();
        final Optional<Reservation> optionalReservation = userAccount.getReservationList().stream().filter(reservation -> reservation.getBook().getBookId().equals(bookId)).findAny();
        optionalReservation.ifPresent(reservationRepository::delete);
        if (optionalBorrowing.isPresent()){
            throw new RuntimeException("Vous avez déjà emprunté ce livre");
        }
        if (book.getQuantity() <= 0) {
            throw new RuntimeException("Ce livre n'est plus disponible");
        }
        Borrowing borrowing = new Borrowing();
        borrowing.setUserAccountBorrowing(userAccount);
        borrowing.setBookBorrowing(book);
        borrowing.setBeginDate(LocalDate.now());
        borrowing.setEndDate(LocalDate.now().plusWeeks(4));
        borrowing.setRenewal(false);
        borrowing.setRendering(false);
        book.setQuantity(book.getQuantity() - 1);
        borrowingRepository.save(borrowing);
        bookRepository.save(book);
        userAccount.getBorrowingList().add(borrowing);
        userAccountRepository.save(userAccount);
    }

    @ExcludeFromJacocoGeneratedReport
    public void reservation(int bookId, String userName) {
        UserAccount userAccount = userAccountRepository.findByUsername(userName).get();
        Book book = bookRepository.findBookByBookId(bookId).get();
        List<Reservation> reservationList = reservationRepository.findAllReservationByBook(book);
        final Optional<Borrowing> optionalBorrowing = userAccount.getBorrowingList().stream().filter(b -> b.getBookBorrowing().getBookId().equals(bookId)).findAny();
        final Optional<Reservation> optionalReservation = userAccount.getReservationList().stream().filter(reservation -> reservation.getBook().getBookId().equals(bookId)).findAny();
        if (reservationList.size() > book.getMaxQuantity() * 2) {
            throw new RuntimeException("La réservation de ce livre n'est pas possible. Veuillez revenir plus tard");
        }
        if (optionalBorrowing.isPresent()) {
            throw new RuntimeException("Vous avez déjà emprunté ce livre");
        }
        if (optionalReservation.isPresent()) {
            throw new RuntimeException("Vous avez déjà réservé ce livre");
        }
        Reservation reservation = new Reservation();
        reservation.setUser(userAccount);
        reservation.setBook(book);
        reservation.setDate(LocalDateTime.now());
        book.setReservationWaitingList(reservation.getBook().getReservationWaitingList());
        reservationRepository.save(reservation);
        book.getReservationWaitingList().add(reservation);
        bookRepository.save(book);
        userAccount.getReservationList().add(reservation);
        userAccountRepository.save(userAccount);
    }

    public void deleteReservation(int reservationId) {
        reservationRepository.deleteById(reservationId);
    }

    @ExcludeFromJacocoGeneratedReport
    public void deleteBorrow(int borrowingId) {
        Borrowing borrowing = borrowingRepository.findById(borrowingId).get();
        borrowing.setRendering(true);
        borrowing.setRenewal(false);
        borrowing.getBookBorrowing().setQuantity(+1);
        manageReservations(borrowing.getBookBorrowing());
        borrowingRepository.deleteById(borrowing.getBorrowingId());
    }

    @ExcludeFromJacocoGeneratedReport
    public void manageReservations(Book book){
        if (book.getReservationWaitingList().size() > 0) {
            Reservation reservation = book.getReservationWaitingList()
                    .get(0);
            reservation.setMailSentDate(LocalDateTime.now());
            reservation.setMailSent(true);
            sendMailReservation(reservation.getUser(), book);
        }
    }

    public void update(Integer borrowingId) {
        Borrowing borrowing = borrowingRepository.findById(borrowingId).orElseThrow(() -> new RuntimeException("La période de rallonge d'emprunt a déjà été effectuée"));
        if (borrowing.getEndDate().isAfter(LocalDate.now())) {
            borrowing.setEndDate(borrowing.getEndDate().plusWeeks(4));
            borrowing.setRenewal(true);
            borrowingRepository.save(borrowing);
        } else {
            throw new RuntimeException("Le prêt ne peut pas être renouvelé");
        }
    }

    @ExcludeFromJacocoGeneratedReport
    public List<BatchDto> findLateBorrowingDate() {
        List<Borrowing> list = borrowingRepository.findBorrowingByEndDateAndUserAccountBorrowing(LocalDate.now());
        HashMap<UserAccount, List<Borrowing>> map = list.stream().collect(groupingBy(Borrowing::getUserAccountBorrowing, HashMap::new, toList()));
        return map.entrySet().stream().map(this::mapBatchDto).collect(toList());
    }

    @ExcludeFromJacocoGeneratedReport
    public void deleteOldReservation() {
        List<Reservation> reservationList = reservationRepository.findReservationByMailSentTrueAndMailSentDateBefore();
        reservationList.forEach(r-> {
            Book book = r.getBook();
            reservationRepository.delete(r);
            manageReservations(book);
        });
    }

    @ExcludeFromJacocoGeneratedReport
    private BatchDto mapBatchDto(Map.Entry<UserAccount, List<Borrowing>> entry) {
        BatchDto batchDto = new BatchDto();
        batchDto.setUsername(entry.getKey().getUsername());
        batchDto.setEmail(entry.getKey().getEmail());
        batchDto.setBorrowings(entry.getValue().stream().map(BorrowingDtoMapper::toDto).collect(toList()));
        return batchDto;
    }

    @ExcludeFromJacocoGeneratedReport
    public void sendMail(BatchDto batchDto) {
        String text = "Bonjour Monsieur " + batchDto.getUsername() + ", \n" +
                "N'oubliez pas de rendre les emprunts suivants : \n" +
                batchDto.getBorrowings().stream().
                        map(b -> b.getBookBorrowing().getTitle() + ", " + "Auteur : " + b.getBookBorrowing().getBookAuthor().getFirstName() + " " + b.getBookBorrowing().getBookAuthor().getLastName())
                        .reduce((a, b) -> a + "\n" + b).orElseGet(() -> "") +
                "\nCordialement";
        MailObject mailObject = new MailObject();
        mailObject.setTo(batchDto.getEmail());
        mailObject.setSubject("Late loan");
        mailObject.setText(text);
        emailServiceImpl.sendSimpleMessage(mailObject.getTo(), mailObject.getSubject(), mailObject.getText());
    }

    @ExcludeFromJacocoGeneratedReport
    public void sendMailReservation(UserAccount userAccount, Book book) {

        String text = "Bonjour Monsieur " + userAccount.getUsername() + ", \n" +
                "Le livre que vous aviez réservé : \n" +
                book.getReservationWaitingList().stream().
                        map(b -> b.getBook().getTitle() + ", " + "Auteur : " + b.getBook()
                                .getBookAuthor().getFirstName() + " " + b.getBook().getBookAuthor().getLastName())
                        .reduce((a, b) -> a + "\n" + b).orElseGet(() -> "") +
                " est à nouveau disponible \n" +
                "\nCordialement";
        MailObject mailObject = new MailObject();
        mailObject.setTo(userAccount.getEmail());
        mailObject.setSubject("Loan possible");
        mailObject.setText(text);
        emailServiceImpl.sendSimpleMessage(mailObject.getTo(), mailObject.getSubject(), mailObject.getText());
    }
}