package com.aubrun.eric.projet7.business.dto;

import com.aubrun.eric.projet7.beans.annotations.ExcludeClassFromJacocoGeneratedReport;

import java.time.LocalDate;
import java.time.LocalDateTime;

@ExcludeClassFromJacocoGeneratedReport
public class ReservationDto {

    private Integer reservationId;
    private BookDto book;
    private UserAccountDto user;
    private LocalDateTime date;
    private LocalDateTime mailSentDate;
    private Boolean mailSent;
    private Integer position;
    private LocalDate firstReturnDate;

    public Integer getReservationId() {
        return reservationId;
    }

    public void setReservationId(Integer reservationId) {
        this.reservationId = reservationId;
    }

    public BookDto getBook() {
        return book;
    }

    public void setBook(BookDto book) {
        this.book = book;
    }

    public UserAccountDto getUser() {
        return user;
    }

    public void setUser(UserAccountDto user) {
        this.user = user;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public LocalDateTime getMailSentDate() {
        return mailSentDate;
    }

    public void setMailSentDate(LocalDateTime mailSentDate) {
        this.mailSentDate = mailSentDate;
    }

    public Boolean getMailSent() {
        return mailSent;
    }

    public void setMailSent(Boolean mailSent) {
        this.mailSent = mailSent;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public LocalDate getFirstReturnDate() {
        return firstReturnDate;
    }

    public void setFirstReturnDate(LocalDate firstReturnDate) {
        this.firstReturnDate = firstReturnDate;
    }
}
