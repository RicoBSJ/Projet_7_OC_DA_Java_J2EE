package com.aubrun.eric.projet7.business.service;

import com.aubrun.eric.projet7.beans.annotations.ExcludeClassFromJacocoGeneratedReport;

@ExcludeClassFromJacocoGeneratedReport
public interface EmailService {

    default void sendSimpleMessage(String to, String subject, String text){

    }
}
