package com.aubrun.eric.projet7.beans;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@SequenceGenerator(name = "reservation_id_generator", sequenceName = "reservation_id_seq", allocationSize = 1)
@Table(name = "RESERVATION")
public class Reservation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "reservation_id_generator")
    @Column(name = "id_reservation")
    private Integer reservationId;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "reservation_book")
    private Book book;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "reservation_user")
    private UserAccount user;
    @Column(name = "reservation_date")
    private LocalDateTime date;
    @Column(name = "reservation_mail_sent_date")
    private LocalDateTime mailSentDate;
    @Column(name = "reservation_mail_sent")
    private Boolean mailSent;

    public Reservation() {
    }

    public Integer getReservationId() {
        return reservationId;
    }

    public void setReservationId(Integer reservationId) {
        this.reservationId = reservationId;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public UserAccount getUser() {
        return user;
    }

    public void setUser(UserAccount user) {
        this.user = user;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public LocalDateTime getMailSentDate() {
        return mailSentDate;
    }

    public void setMailSentDate(LocalDateTime mailSentDate) {
        this.mailSentDate = mailSentDate;
    }

    public Boolean getMailSent() {
        return mailSent;
    }

    public void setMailSent(Boolean mailSent) {
        this.mailSent = mailSent;
    }

    @Override
    public String toString() {
        return "Reservation{" +
                "reservationId=" + reservationId +
                ", book=" + book +
                ", user=" + user +
                ", date=" + date +
                ",mailSentDate=" + mailSentDate +
                ",mailSent=" + mailSent +
                '}';
    }
}
