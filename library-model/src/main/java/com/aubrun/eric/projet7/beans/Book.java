package com.aubrun.eric.projet7.beans;

import javax.persistence.*;
import java.util.List;

@Entity
@SequenceGenerator(name = "book_id_generator", sequenceName = "book_id_seq", allocationSize = 1)
@Table(name = "BOOK")
public class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "book_id_generator")
    @Column(name = "id_book")
    private Integer bookId;
    @Column(name = "book_title")
    private String title;
    @Column(name = "book_quantity")
    private Integer quantity;
    @Column(name = "book_max")
    private Integer maxQuantity;
    @Column(name = "book_year")
    private String yearBook;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_author")
    private Author bookAuthor;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_category")
    private Category bookCategory;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_edition")
    private Edition bookEdition;
    @OneToMany
    @JoinColumn(name = "reservation_waiting_list")
    private List<Reservation> reservationWaitingList;

    public Book() {
    }

    public Integer getBookId() {
        return bookId;
    }

    public void setBookId(Integer bookId) {
        this.bookId = bookId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getMaxQuantity() {
        return maxQuantity;
    }

    public void setMaxQuantity(Integer maxQuantity) {
        this.maxQuantity = maxQuantity;
    }

    public String getYearBook() {
        return yearBook;
    }

    public void setYearBook(String yearBook) {
        this.yearBook = yearBook;
    }

    public Author getBookAuthor() {
        return bookAuthor;
    }

    public void setBookAuthor(Author bookAuthor) {
        this.bookAuthor = bookAuthor;
    }

    public Category getBookCategory() {
        return bookCategory;
    }

    public void setBookCategory(Category bookCategory) {
        this.bookCategory = bookCategory;
    }

    public Edition getBookEdition() {
        return bookEdition;
    }

    public void setBookEdition(Edition bookEdition) {
        this.bookEdition = bookEdition;
    }

    public List<Reservation> getReservationWaitingList() {
        return reservationWaitingList;
    }

    public void setReservationWaitingList(List<Reservation> reservationWaitingList) {
        this.reservationWaitingList = reservationWaitingList;
    }

    @Override
    public String toString() {
        return "Book{" +
                "bookId=" + bookId +
                ", title='" + title + '\'' +
                ", quantity=" + quantity +
                ", maxQuantity=" + maxQuantity +
                ", yearBook=" + yearBook +
                ", bookAuthor=" + bookAuthor +
                ", bookCategory=" + bookCategory +
                ", bookEdition=" + bookEdition +
                ", reservationWaitingList=" + reservationWaitingList +
                '}';
    }
}