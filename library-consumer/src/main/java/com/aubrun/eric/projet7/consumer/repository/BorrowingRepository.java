package com.aubrun.eric.projet7.consumer.repository;

import com.aubrun.eric.projet7.beans.Book;
import com.aubrun.eric.projet7.beans.Borrowing;
import com.aubrun.eric.projet7.beans.UserAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

@Repository
public interface BorrowingRepository extends JpaRepository<Borrowing, Integer> {
    @Query("SELECT b FROM Borrowing b WHERE b.userAccountBorrowing = :userAccount")
    List<Borrowing> findAllUserBorrowing(UserAccount userAccount);

    @Query("SELECT b FROM Borrowing b WHERE b.endDate <= :date")
    List<Borrowing> findBorrowingByEndDateAndUserAccountBorrowing(LocalDate date);

    @Query(value = "SELECT borrowing_end_date FROM Borrowing WHERE id_book = :idBook AND book_rendering = false ORDER BY borrowing_end_date ASC LIMIT 1", nativeQuery = true)
    Date findBorrowingByEndDateAndAndBookBorrowing(Integer idBook);
}