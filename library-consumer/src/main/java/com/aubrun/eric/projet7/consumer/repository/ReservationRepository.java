package com.aubrun.eric.projet7.consumer.repository;

import com.aubrun.eric.projet7.beans.Book;
import com.aubrun.eric.projet7.beans.Reservation;
import com.aubrun.eric.projet7.beans.UserAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.util.List;

@Repository
public interface ReservationRepository extends JpaRepository<Reservation, Integer> {

    @Query("SELECT r FROM Reservation r WHERE r.user = :userAccount")
    List<Reservation> findAllUserReservation(UserAccount userAccount);

    @Query("SELECT r FROM Reservation r WHERE r.book = :book")
    List<Reservation> findAllReservationByBook(Book book);

    @Query(value = "SELECT * FROM reservation WHERE reservation_mail_sent = true AND reservation_mail_sent_date < (now() - INTERVAL '2' DAY)", nativeQuery = true)
    List<Reservation> findReservationByMailSentTrueAndMailSentDateBefore();
}