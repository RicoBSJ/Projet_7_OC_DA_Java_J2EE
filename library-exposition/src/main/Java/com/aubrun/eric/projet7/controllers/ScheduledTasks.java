package com.aubrun.eric.projet7.controllers;

import com.aubrun.eric.projet7.business.service.BorrowingService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ScheduledTasks {

    private final BorrowingService borrowingService;

    public ScheduledTasks(BorrowingService borrowingService) {
        this.borrowingService = borrowingService;
    }

    //@Scheduled(fixedRate = 172800000)
    @Scheduled(fixedDelay = 180000, initialDelay = 300000)
    public void deleteOldReservations(){
        borrowingService.deleteOldReservation();
    }
}
