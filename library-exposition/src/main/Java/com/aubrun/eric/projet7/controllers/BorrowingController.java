package com.aubrun.eric.projet7.controllers;

import com.aubrun.eric.projet7.beans.Borrowing;
import com.aubrun.eric.projet7.business.dto.BatchDto;
import com.aubrun.eric.projet7.business.dto.BookDto;
import com.aubrun.eric.projet7.business.dto.BorrowingDto;
import com.aubrun.eric.projet7.business.dto.ReservationDto;
import com.aubrun.eric.projet7.business.service.BorrowingService;
import com.aubrun.eric.projet7.business.service.UserAccountService;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/borrowings")
public class BorrowingController {

    private final BorrowingService borrowingService;

    public BorrowingController(BorrowingService borrowingService, UserAccountService userAccountService) {
        this.borrowingService = borrowingService;
    }

    @GetMapping("/")
    public List<BorrowingDto> getAllBorrowings(Principal principal) {
        return this.borrowingService.findAll(principal);
    }

    @GetMapping("/allReserves")
    public List<ReservationDto> getAllReserves(Principal principal) {
        return this.borrowingService.findAllReserves(principal);
    }

    @GetMapping("/{id}")
    public BorrowingDto getBorrowingById(@PathVariable(value = "id") int borrowingId) {
        return this.borrowingService.findById(borrowingId);
    }

    @PostMapping("/")
    public void createBorrowing(Principal principal, @RequestBody BookDto bookDto) {
        borrowingService.save(bookDto.getBookId(), principal.getName());
    }

    @PostMapping("/reserve")
    public void reserveBook(Principal principal, @RequestBody BookDto bookDto) {
        borrowingService.reservation(bookDto.getBookId(), principal.getName());
    }

    @PutMapping("/{id}")
    public void updateBorrowing(@PathVariable(value = "id") int borrowingId) {
        borrowingService.update(borrowingId);
    }

    @GetMapping("/lateDate")
    public List<BatchDto> lateBorrowingDate() {
        return this.borrowingService.findLateBorrowingDate();
    }

    @GetMapping("/lateReservation")
    public void lateReservationDate() {
        borrowingService.deleteOldReservation();
    }

    @DeleteMapping(value = "/{id}")
    public void deleteReservation(@PathVariable("id") int reservationId) {
        borrowingService.deleteReservation(reservationId);
    }

    @DeleteMapping(value = "/reserve")
    public void deleteReservations() {
        borrowingService.deleteOldReservation();
    }

    @DeleteMapping(value = "/delBorrow/{id}")
    public void deleteBorrow(@PathVariable("id") int borrowingId) {
        borrowingService.deleteBorrow(borrowingId);
    }
}